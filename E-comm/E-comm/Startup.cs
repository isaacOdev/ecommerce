﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(E_comm.Startup))]
namespace E_comm
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
